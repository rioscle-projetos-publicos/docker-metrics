# Projeto Métricas

Neste projeto foi utilizado o docker-compose para criar um ambiente para integrar API + Prometheus + Grafana

Foi utilizado o NGINX como um gateway/proxy-reverso para acessar os endpoints da API.

 - localhost/api ( método get que vai gerar informações para as métricas )
 - localhost/api/metrics ( métricas criadas para a API )

A API foi criado utilizando **Python**.

O **Prometheus** é acessodo através da porta 9090. 

 - No arquivo "prometheus/prometheus.yml", em targets, informar o IP da sua máquina
 - As configurações são persistidas neste arquivo e é recriado toda a vez junto com o docker-compose

O **Grafana** é acessado através da porta 3000

 - As configurações de "data source" do prometheus são persistidas no arquivo e é recriado toda a vez junto com o docker-compose.

## Arquitetura Básica do Projeto

```mermaid
graph LR
A(NGINX) --> B(APP)
C(Prometheus) --> B
D(Grafana) --> C
```

## Arquitetura Básica do Projeto

### endpoints e urls
- http://localhost/api: incrementa os contadores do prometheus
- http://localhost/api/metrics: visualiza as metricas do prometheus que a api expoe
- http://localhost/9090: abre a interface do prometheus
- http://localhost/3000: abre a interface do grafana


# Prometheus

Documentação Oficial do Prometheus: [Link](https://prometheus.io/docs/prometheus/latest/querying/basics/)

Neste link temos alguns exemplos práticos do prometheus: [Link](https://medium.com/ifood-tech/monitoramento-de-servi%C3%A7os-com-prometheus-f8c4071c80c4)

## Tipos de Métricas
No prometheus existem 4 tipos de métricas: counter, gauge, histogram, summary.
Documentação do Prometheus sobre os tipos de métricas: [Link](https://prometheus.io/docs/concepts/metric_types/)

Documentação bem detalhada dos tipos de métricas: [Link](https://tomgregory.com/the-four-types-of-prometheus-metrics/)

### Counter: 
Um Contador que o valor só aumenta
No restart da aplicação o valor pode zerar, pois é armazenado na mémória da aplicação. 

![Exemplo Counter](/images/counter_example.png "Counter")

### Gauge
Pode aumentar ou diminuir arbitrariamente

![Exemplo Gauge](/images/gauge_example.png "Gauge")
 
### Histogram
Usado para medir a distribuiçao de requisições por buckets  ( tempo especifico ).
Mostra a quantidade de eventos que aconteram um periodo de tempo.
Exemplo: 500 requisições responderam em até 100 ms
Esta Metrica expoe:
- A quantidade de requisições por bucket: `<basename>_bucket`
- Tempo total de requisições de todas as requisições: `<basename>_sum`
- Total de requisições: `<basename>_count`

Obs: com esta metrica também expoe o total de requisições que estão chegando ( como a métrica Couter )

### Summary
Usado para medir a distribuiçao.
Mostra o "percentil" da frequencia das requisicoes.
Exemplo: 99% das requisições responderam em até 500 ms.
Esta Metrica expoe:
- O tempo por quantil: `<basename>{quantile="<φ>"}`
- Tempo total de todas as requisições: `<basename>_sum`
- Total de requisições: `<basename>_count`

## Funções

Documentação do Prometheus sobre os funções: [Link](https://prometheus.io/docs/prometheus/latest/querying/functions/#rate)

### rate
Mostra a média de requisições por segundo
O exemplo abaixo está sendo calculado a média de requisições por segundo dos ultimos 5 minutos: 
`rate(http_requests_total{job="api-server"}[5m])`

### increase
Calcula o aumento em um intervalo.
**Increase** deve ser utilizado somente com contadores.
O exemplo abaixo está sendo calculado calculado a somada de requisições do ultimo 1 minuto: 
`increase(http_requests_total{job="api-server"}[1m])`

Obs: O **increase** utiliza a média de requisições por segundo calculado pela funçao **rate** e multiplica pelo tempo ( range vector ). Abaixo podemos ver como que o exemplo acima utilizando a função "rate":
`rate(http_requests_total{job="api-server"}[1m])*60`

### histogram_quantile
Calcula o tempo de duracao de um bucket especifico ( quantil ). Esta funcao calcuma o quantil através de uma métrica do tipo Histogram

No exemplo abaixo, podemos ver a media de tempo das requisições do quantil 90. Ou seja, assim sabemos que 90% das requisições estao executando em até o valor do tempo calculado: 
`histogram_quantile(0.9, rate(http_request_duration_seconds_bucket[10m]))`

### Calculo do tempo médio de requisições
Para o calculo do tempo médio de requisições podemos utilizar `<basename>_sum` e `<basename>_count` das métricas **Histogram** e **Summary**.
Exemplo:  `increase(http_requests_time_secounds_sum[1m]) / increase(http_requests_time_secounds_count[1m])`

### Comparar uma métrica com um tempo no passado

O operador `offset` busca no periodo estabelecido o valor da métrica no passado
Documentação do Prometheus: [Link](https://prometheus.io/docs/prometheus/latest/querying/basics/#offset-modifier)

Exemplos:
- Soma das requisições em um minuto, 5 minutos atrás: `increase(http_requests_total{job="api-server"}[1m] offset 5m)`
- Soma de requisições em um minuto, 1 hora atrás: `increase(http_requests_total{job="api-server"}[1m] offset 1h)`
- Soma de requisições em um minuto, 7 dias atrás: `increase(http_requests_total{job="api-server"}[1m] offset 7d)`
- Soma de requisições em um minuto, 2 semanas atrás: `increase(http_requests_total{job="api-server"}[1m] offset 2w)`

#### Time Durations

Documentação do Prometheus: [Link](https://prometheus.io/docs/prometheus/latest/querying/basics/#time-durations)
- **ms** - milliseconds
- **s** - seconds
- **m** - minutes
- **h** - hours
- **d** - days - assuming a day has always 24h
- **w** - weeks - assuming a week has always 7d
- **y** - years - assuming a year has always 365d


## Operações Aritméticas e Operadores de Agregação

Documentação do Prometheus: [Link](https://prometheus.io/docs/prometheus/latest/querying/operators/)

### Operações Aritméticas

- **+** (addition)
- **-** (subtraction)
- **\*** (multiplication)
- **/** (division)
- **%** (modulo)
- **^** (power/exponentiation)

### Operações de Agregação

Através de uma métrica podemos:
- **sum** (calculate sum over dimensions)
- **min** (select minimum over dimensions)
- **max** (select maximum over dimensions)
- **avg** (calculate the average over dimensions)
- **group** (all values in the resulting vector are 1)
- **stddev** (calculate population standard deviation over dimensions)
- **stdvar** (calculate population standard variance over dimensions)
- **count** (count number of elements in the vector)
- **count_values** (count number of elements with the same value)
- **bottomk** (smallest k elements by sample value)
- **topk** (largest k elements by sample value)
- **quantile** (calculate φ-quantile (0 ≤ φ ≤ 1) over dimensions)
