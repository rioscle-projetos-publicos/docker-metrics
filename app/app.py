# flask utilizado para criar uma API no Python
from flask import Response, Flask, request

# dependencias do prometheus
import prometheus_client
from prometheus_client.core import CollectorRegistry
from prometheus_client import Summary, Counter, Histogram, Gauge
import time
import random

app = Flask(__name__)

_INF = float("inf")

graphs = {}
graphs['c'] = Counter('app_request_operations_total', 'The total number of processed requests', ['method', 'endpoint', 'httpStatus'])
graphs['h'] = Histogram('app_histogram_duration_seconds', 'Histogram for the duration in seconds', ['method', 'endpoint', 'httpStatus'], buckets=(0.5, 0.7, 0.8, 0.9, 0.95, 0.99, _INF))
graphs['s'] = Summary('app_summary_duration_seconds', 'Summary for the duration in seconds', ['method', 'endpoint', 'httpStatus'])

@app.route("/")
def hello():
    
    random_Method = randomMethod(random1to10())
    random_Status = randomStatus(random1to10())
    graphs['c'].labels(method=random_Method, endpoint='/', httpStatus=random_Status).inc()
    
    random_time = getMS()
    graphs['h'].labels(method=random_Method, endpoint='/', httpStatus=random_Status).observe(random_time)
    graphs['s'].labels(method=random_Method, endpoint='/', httpStatus=random_Status).observe(random_time)
    
    return "Request Processada"

@app.route("/metrics")
def requests_count():
    res = []
    for k,v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype="text/plain")

def getMS():
    if random1to10() <= 8: 
        return (random.randint(1,5)) * 0.10
    else:
        return (random.randint(3,6)) * 0.35
    

def random1to10():
    return (random.randint(1,10))

def randomMethod(random_Number):
    if random_Number <= 5: return 'get'
    if random_Number > 5 and random_Number <= 8 : return 'put'
    if random_Number > 8: return 'delete'

def randomStatus(random_Number):
    if random_Number <= 7: return '200'
    if random_Number > 7 and random_Number <= 9 : return '400'
    if random_Number > 9: return '500'